package com.greenhills.database;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import android.content.Context;
import android.util.Log;

public class ServerTaskHelper {

	/*
	 * ------------------------------------- CLASS VARIABLES -----------------------------------//
	 */


	private URL url; 	 //Servers URL
	private Context context;

	//Login task keys
	private final String sessionIdHex = "73657373696F6E4944";
	private final String userIdHex = "757365724944";
	private final String authHex = "41757468"; //- check returned by server
	private final String authHexSuccessful = "5375636365737366756C"; //- hex value for Successful Authentication

	//Verify session keys
	private final String sessionValid = "53657373696F6E56616C6964";
	private final String sessionValidTrue = "54727565";
	private final String sessionValidFalse="46616C7365";
	
	//Access data from server - Groups
	private final String groupName ="67726F75704E616D65"; //Group Name
	private final String groupCreatedTime="67726F75704372656174656454696D65";
	private final String beaconMac="626561636F6E4D4143";
	private final String beaconUUID="626561636F6E55554944";
	private final String beaconMajor="626561636F6E4D616A6F72";
	private final String beaconMinor="626561636F6E4D696E6F72";
	
	//Access data from server - StaticLocation
	
	private final String attractionId="61747472616374696F6E4944";
	private final String attractionDuration="6475726174696F6E";
	private final String attractionCapacity="6361706163697479";
	private final String attractionName="706C6163654E616D65";
	private final String attractionDescription="6465736372697074696F6E";
	private final String openTime="6F70656E54696D65";
	private final String closeTime="636C6F736554696D65";
	
	//final variables for selection of database table to add
	private final static String selectShows="shows";
	private final static String selectEateries="eateries";
	private final static String selectShops="shops";
	private final static String selectFacilities="facilities";
	




	/*
	 * ------------------------------------- CLASS CONSTRUCTORS --------------------------------//
	 */

	public ServerTaskHelper(Context context){
		
		try{
			this.url = new URL("http://isb.phoxenix.com/index.php?");
		} catch(MalformedURLException e){
			e.printStackTrace();
		}
		this.context=context;
	}

	/*
	 * ------------------------------------- CLASS METHODS -------------------------------------//
	 */
	
	// ------------------------------------- HTTP POST AND GET from server

	public String[] getServerResponse(String data) {
		
		String serverResponse="";
		
		// Send POST data request
		try {
			
			URLConnection conn = url.openConnection();
			conn.setDoOutput(true);
			OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
			wr.write(data);
			wr.flush();

			// Get the server response

			BufferedReader reader = new BufferedReader(new InputStreamReader(
					conn.getInputStream()));
			StringBuilder sb = new StringBuilder();
			String line = null;

			// Read Server Response
			while ((line = reader.readLine()) != null) {
				// Append server response in string
				sb.append(line + "\n");
			}
			serverResponse = sb.toString();
			
			reader.close();

		} catch (Exception e){
			e.printStackTrace();
		}

		String delimiter = "[:;]";
		String[] tokens = serverResponse.split(delimiter);
		
		Log.i("Server Response", serverResponse);
		
		return tokens;
	}
	
	// ------------------------------------- Login Authentication
	// returns  true if login was successfull.
	
	public boolean authenticateUser(String[] tokens){
		
		boolean userIdFound = false;
		boolean sessionIdFound = false;
		boolean loginSuccess = false;
		String convertSessionId = "";
		String convertUserIdHex = "";
		
		GlobalDatabase dbState= ((GlobalDatabase) context.getApplicationContext());
		
		for( int i = 0; i < tokens.length; i++)
  		{
				if(tokens[i].equals(sessionIdHex))
  			{
  				convertSessionId = tokens[i+1];
  				StringBuilder output = new StringBuilder();
		            for (int j = 0; j < convertSessionId.length(); j+=2) 
		            {
		            	String str = convertSessionId.substring(j, j+2);
		                output.append((char)Integer.parseInt(str, 16));
		            }
  				
		         //Set Users sessionKey variable on GlobalDatabase class
		         dbState.getUser().setSessionKey(output.toString());
  				sessionIdFound = true;
  			}
  			else if (tokens[i].equals(userIdHex))
  			{
  				convertUserIdHex = tokens[i+1];
  				StringBuilder output = new StringBuilder();
		            for (int j = 0; j < convertUserIdHex.length(); j+=2) 
		            {
		            	String str = convertUserIdHex.substring(j, j+2);
		                output.append((char)Integer.parseInt(str, 16));
		            }
		            
		            //Set Users userId variable on GlobalDatabase class
		            dbState.getUser().setUserId(output.toString());
		            userIdFound = true;
  			}
  			else if (tokens[i].equals(authHex))
  			{
  				if (tokens[i+1].equals(authHexSuccessful))
  				{
  					loginSuccess = true;
  					Log.e("DEBUG", "Login success");
  				}
  				else 
  				{
  					loginSuccess=false;
  					Log.e("DEBUG", "Login Failed");
  					break; //- login wasn't successful so just quit
  				}
  			}
				if (sessionIdFound && userIdFound && loginSuccess)
				{
					break;
				}
  		}
		return loginSuccess;
	} // End of User Authentication Method
	
	// ------------------------------------- Session Validation
	// returns  true if current session is valid.
	
	public boolean verifyCurrentSession(String[] tokens) {
		
		boolean sessionVerified=false;

		for (int i = 0; i < tokens.length; i++) {
			if (tokens[i].equals(sessionValid)) {

				if (tokens[i + 1].equals(sessionValidTrue)) {
					Log.i("GreenHills", "Valid Session Key");
					sessionVerified = true;
					break;

				} else if (tokens[i + 1].equals(sessionValidFalse)) {
					
					Log.i("GreenHills", "Invalid Session, reAuthenticate");
					sessionVerified = false;
					break;
				}
			}
		}
		
		return sessionVerified;
	} // End of verifyCurrentSession Method
	
	// ------------------------------------- SETS USERS GROUPS ON DATABASE
	public void setUserGroups(String[] tokens){
		
		//Database variables
		GlobalDatabase dbState= ((GlobalDatabase) context.getApplicationContext());
		DatabaseHandler db= dbState.getDb();
		
		//Group and beacon Variables
		Group group= new Group();
		Beacon beacon = new Beacon();
		String groupNameControl="";
		
		/**
		 * Loops tokens array to find a valid groupName, it then checks that the same group hasn't
		 * been found on the array to then create the group object and start looking for the correspondent
		 * beacons of each group.
		 */
		for (int i =0; i<tokens.length;i++) {

			if (tokens[i].equals(groupName)){ //Finds name of Group
				
				if (!hexToString(tokens[i+1]).equals(groupNameControl)){ //Checks if group already exists
				group=new Group();
				groupNameControl=hexToString(tokens[i+1]);
				group.setGroupName(groupNameControl); //Sets name of group on database
				
				for (int q=i; q<tokens.length;q++){ //Finds Group created time
					if (tokens[q].equals(groupCreatedTime)){
						group.setGroupCreatedTime(hexToString(tokens[q+1]));
						break;
					}
				}
				
				db.addGroup(group); //Adds group to Database
				} 
				
				for (int j=i; j<tokens.length;j++){ //Looks for Group's beacons

					if(tokens[j].equals(beaconMac)){
						beacon.setBeaconMac(hexToString(tokens[j+1]));
						j++;
					}else if (tokens[j].equals(beaconUUID)){
						beacon.setBeaconUUID(hexToString(tokens[j+1]));
						j++;
					} else if (tokens[j].equals(beaconMajor)){
						beacon.setBeaconMajor(Integer.parseInt(hexToString(tokens[j+1])));
						j++;
					} else if (tokens[j].equals(beaconMinor)){
						beacon.setBeaconMinor(Integer.parseInt(hexToString(tokens[j+1])));
						i=j+1;
						db.addBeaconToDB(beacon);
						db.linkBeaconToGroup(beacon, group, db);
						break; //Exits for when beaconMinor is found (last parameter of interest)
					}
					
				}
			}
		}
	} //End of setUserGroups Method
	
	// ------------------------------------- SETS USER'S ACHIEVEMENTS
		public void setUserAchievements(String[] tokens){
			
			//Database variables
			GlobalDatabase dbState= ((GlobalDatabase) context.getApplicationContext());
			
			/**TODO
			 * 
			 */
			
			for (int i=0; i<tokens.length;i++){
				Log.e("Debugger", hexToString(tokens[i]));
			}
		}
		
	// ------------------------------------- SETS RIDES
	public void setRides(String[] tokens) {
		
		//Database variables
				GlobalDatabase dbState= ((GlobalDatabase) context.getApplicationContext());
				
		StaticLocation sl = new StaticLocation();
		int idControl=-100;

		for (int i = 0; i < tokens.length; i++) {
			
			if (tokens[i].equals(attractionId)){
				
				if (!hexToString(tokens[i+1]).equals(Integer.toString(idControl))); //Checks for a new attraction id
				
				for (int j=i; j<tokens.length;j++){
					if (tokens[j].equals(attractionDuration)){
						sl.setDuration(Integer.parseInt(hexToString(tokens[j+1])));
						j++;
					} else if (tokens[j].equals(attractionCapacity)){
						sl.setCapacity(Integer.parseInt(hexToString(tokens[j+1])));
						j++;
					} else if (tokens[j].equals(attractionName)){
						sl.setName(hexToString(tokens[j+1]));
						j++;
					} else if (tokens[j].equals(attractionDescription)){
						sl.setDetails(hexToString(tokens[j+1]));
						j++;
					} else if (tokens[j].equals(openTime)){
						sl.setOpenTime(hexToString(tokens[j+1]));
						j++;
					} else if (tokens[j].equals(closeTime)){
						sl.setCloseTime(hexToString(tokens[j+1]));
						//Adds Ride after all parameters have been fetched.
						i=j+1;
						dbState.getDb().addRides(sl);
						break;
					}
				}
			}
		}
	}
	
	// ------------------------------------- SETS Static location
		public void setStaticLocation(String[] tokens, String locationType) {
			
			//Database variables
			GlobalDatabase dbState= ((GlobalDatabase) context.getApplicationContext());
					
			StaticLocation sl = new StaticLocation();
			int idControl=-100;
			
			//Location type - in lower case
			String lt= locationType.toLowerCase();

			for (int i = 0; i < tokens.length; i++) {
				
				if (tokens[i].equals(attractionId)){
					
					if (!hexToString(tokens[i+1]).equals(Integer.toString(idControl))); //Checks for a new attraction id
					
					for (int j=i; j<tokens.length;j++){
						
						if (tokens[j].equals(attractionName)){
							sl.setName(hexToString(tokens[j+1]));
							j++;
						} else if (tokens[j].equals(attractionDescription)){
							sl.setDetails(hexToString(tokens[j+1]));
							j++;
						} else if (tokens[j].equals(openTime)){
							sl.setOpenTime(hexToString(tokens[j+1]));
							j++;
						} else if (tokens[j].equals(closeTime)){
							sl.setCloseTime(hexToString(tokens[j+1]));
							//Adds Ride after all parameters have been fetched.
							i=j+1;
							if (lt.equals(selectShows)){
								dbState.getDb().addShow(sl);
							} else if (lt.equals(selectShops)){
								dbState.getDb().addShops(sl);
							} else if (lt.equals(selectFacilities)){
								dbState.getDb().addFacility(sl);
							} else if (lt.equals(selectEateries)){
								dbState.getDb().addEatery(sl);
							}
							break;
						}
					}
				}
			}
		}

	// ------------------------------------- HEX TO STRING METHOD
	public String hexToString(String hex) {
		StringBuilder sb = new StringBuilder();
		char[] hexData = hex.toCharArray();
		for (int count = 0; count < hexData.length - 1; count += 2) {
			int firstDigit = Character.digit(hexData[count], 16);
			int lastDigit = Character.digit(hexData[count + 1], 16);
			int decimal = firstDigit * 16 + lastDigit;
			sb.append((char) decimal);
		}
		return sb.toString();

	}
} //END OF CLASS
