package com.greenhills.database;

public class Group {
	
	/*
	 * CLASS VARIABLES
	 */
	
	private String groupName;
	private String groupCreatedTime;
	private int id;
	
	
	/*
	 * CONSTRUCTORS
	 */
	
	public Group(){
		this.groupName="";
		this.groupCreatedTime="";
		this.id=0;
	}
	
	public Group(String name, String date){
		this.groupName=name;
		this.groupCreatedTime=date;
	}
	
	/*
	 * ACCESSORS
	 */
	
	public String getGroupName(){
		return this.groupName;
	}
	
	public String getGroupCreatedTime(){
		return this.groupCreatedTime;
	}
	
	public int getId(){
		return this.id;
	}
	
	/*
	 * MUTATORS
	 */
	
	public void setGroupName(String name){
		this.groupName=name;
	}
	
	public void setGroupCreatedTime(String time){
		this.groupCreatedTime=time;
	}
	
	public void setId(int i){
		this.id=i;
	}
	
	/*
	 * CLASS METHODS
	 */
	
	@Override
	public String toString(){
		return " Name: "+groupName+" Crated: "+groupCreatedTime;
	}

}
