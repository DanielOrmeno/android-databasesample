package com.greenhills.database;

public class PlannedStaticLocation extends StaticLocation {
	
	//Class variables
	String scheduledTime;
	
	public PlannedStaticLocation(){
		super ();
	}
	
	public PlannedStaticLocation(StaticLocation l){
		super(l.getUUID(), l.getName(), l.getDetails(), l.getLatitude(),l.getLongitude(),l.getIcon());
		this.scheduledTime="";
	}
	
	// Accessors and Mutators
	
	public String getScheduledTime(){
		return scheduledTime;
	}
	
	public void setScheduledTime(String s){
		this.scheduledTime=s;
	}
}
