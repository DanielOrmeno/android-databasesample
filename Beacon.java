package com.greenhills.database;

public class Beacon {
	
	/*
	 * Class Variables
	 */
	
	private String beaconMac;
	private String beaconUUID;
	private String beaconType;
	private int beaconMajor;
	private int beaconMinor;
	private String beaconCreatedTime;
	private String beaconLastUpdatedTime;
	private int beaconId;
	private int beaconRssi;
	
	//Default Constructor
	public Beacon(){
		beaconUUID=null;
		beaconMac=null;
		beaconType=null;
		beaconMajor=0;
		beaconMinor=0;
		beaconCreatedTime=null;
		beaconLastUpdatedTime=null;
		beaconId=0;
		beaconRssi=0;
	}
	
	//Constructor
	public Beacon(String mac, String uuid, int major, int minor, int rssi){
		this.beaconMac=mac;
		this.beaconUUID=uuid;
		//this.beaconType=type;
		this.beaconMajor=major;
		this.beaconMinor=minor;
		//this.beaconCreatedTime=created;
		//this.beaconLastUpdatedTime=updated;
		this.beaconId=0;
		this.beaconRssi=rssi;
		
		// ADD MISSING PARAMETERS IF IMPLEMENTING OTHER VALUES
	}
	
	// ACCESSORS - IMPLEMENT OTHER ACCESSORS IF NEEDED
	
	public String getBeaconMac(){
		return this.beaconMac;
	}
	
	public String getBeaconUUID(){
		return this.beaconUUID;
	}
	
	public int getBeaconMajor(){
		return this.beaconMajor;
	}
	
	public int getBeaconMinor(){
		return this.beaconMinor;
	}
	
	public int getBeaconId(){
		return this.beaconId;
	}
	
	public int getBeaconRssi(){
		return this.beaconRssi;
	}
	
	// MUTATORS - IMPLEMENT OTHER MUTATORS IF NEEDED
	
	public void setBeaconMac(String in){
		beaconMac=in;
	}
	
	public void setBeaconUUID(String in){
		beaconUUID=in;
	}
	
	public void setBeaconMajor(int in){
		beaconMajor=in;
	}
	
	public void setBeaconMinor(int in){
		beaconMinor=in;
	}
	
	public void setBeaconId(int i){
		beaconId=i;
	}
	
	public void setBeaconRssi(int i){
		beaconRssi=i;
	}
	
	@Override
	public String toString(){
		return " Mac: "+beaconMac+" UUID: "+beaconUUID+" Major: "+beaconMajor+" Minor: "+beaconMinor;
	}
}
