package com.greenhills.database;


public class Location {
	
/* ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: CLASS VARIABLES ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/	
	
	private double latLong;
	private double altitude;
	private double accuracy;
	
	
	/* ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: CONSTRUCTORS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	
	public Location(){

		latLong=0;
		altitude=0;
		accuracy=0;
	}
	
	public Location(double ll, double al, double acc){

		this.latLong=ll;
		this.altitude=al;
		this.accuracy=acc;
	}
	
	/* ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: ACCESORS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	
	public double getAccuracy(){
		return this.accuracy;
	}
	
	public double getLatLong(){
		return this.latLong;
	}
	
	public double getAlt(){
		return this.altitude;
	}
	
	/* ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: MUTATORS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	
	public void setAccuracy(double acc){
		this.accuracy=acc;
	}
	
	public void setLatLong(double n){
		this.latLong=n;
	}
	
	public void setAltitude(double n){
		this.altitude=n;
	}
	
	/* ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: CLASS METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
}
