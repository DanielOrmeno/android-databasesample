package com.greenhills.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHandler extends SQLiteOpenHelper {

	/*
	 * Class Variables
	 */

	// Database name & version
	private static final String LOG = "DatabaseHandler";
	private static final int DATABASE_VERSION = 1;
	private static final String DATABASE_NAME = "greenhills_user_db";

	/*
	 * ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	 * ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: TABLES AND KEYS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	 * ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	 */

	// TABLES
	private static final String TABLE_BEACON = "beacon";
	private static final String TABLE_GROUP = "myGroup";
	private static final String TABLE_BEACON_GROUP_LINK = "beaconGroupLink";
	/**
	 * private static final String TABLE_USER = "user"; - See user variables
	 * comment for information about this table
	 */
	private static final String TABLE_LOCATION = "location";
	private static final String TABLE_BEACON_LOC_LINK = "beaconLocationLink";
	private static final String TABLE_PLACES = "places";
	private static final String TABLE_FACILITIES = "facilities";
	private static final String TABLE_RIDES = "rides";
	private static final String TABLE_EATERIES = "eateries";
	private static final String TABLE_SHOWS = "shows";
	private static final String TABLE_SHOPS = "shops";

	private static final String TABLE_ACHIEVEMENTS = "achievements";

	// FOREING KEYS
	// TABLE_BEACON_GROUP_LINK references TABLE_GROUP
	private static final String KEY_GROUP_ID = "groupId";
	// TABLE_BEACON_GROUP_LINK references TABLE_BEACON
	private static final String KEY_BEACON_ID = "beaconId";
	// TABLE_BEACON_LOCATION_LINK references TABLE_BEACON
	private static final String KEY_BEACON_LOC_LINK_BEACON = "beaconId";
	// TABLE_USER references TABLE_LOCATION
	private static final String KEY_USER_LOCATION_ID = "locationId";
	// TABLE FACILITIES,EATERIES,SHOWS,RIDES references TABLE_PLACES
	private static final String KEY_PLACES_ID = "placeId";

	private static final String KEY_LOCATION_ID = "locationId";

	/**
	 * As the user is now stored on the GlobalDatabase class (only one user will
	 * ever be needed per session) this table will is no longer needed (for the
	 * time being), the variables related to this table are left commented as it
	 * may be needed for further implementations of this project.
	 * 
	 * // TABLE_USER private static final String KEY_USER_ID = "userId"; private
	 * static final String KEY_USER_NAME = "userName"; private static final
	 * String KEY_USER_TYPE = "userType"; private static final String
	 * KEY_USER_MOBILE_PHONE = "mobilePhone"; // NOT// SURE IF NEEDED - if
	 * uncommented add to create statement: // private static final String
	 * KEY_USER_PROFILE_PIC="profileFoto";
	 */

	// TABLE_LOCATION
	private static final String KEY_LOC_LAT = "locationLat";
	private static final String KEY_LOC_LONG = "locationLong";
	private static final String KEY_LOC_ALT = "locationAlt";
	private static final String KEY_LOC_ACCURACY = "locationAccuracy";
	// private static final String KEY_LOC_ICON="locationIcon"// needs to be
	// implemented on create statement and methods
	// NOT SURE IF NEEDED if uncommented add to create statement:
	// private static final String KEY_LOCATION_POLL_TIME

	// TABLE_BEACON_LOCATION_LINK
	private static final String KEY_BEACON_LOC_ID = "linkId";
	private static final String KEY_BEACON_LOC_OBJECT_ID = "objectId";
	// NOT SURE IF NEEDED- if uncommented add to create statement:
	// private static final String
	// KEY_BEACON_LOC_CREATED_TIME="locationLinkCreatedTime";

	// TABLE_BEACON
	private static final String KEY_BEACON_MAC = "beaconMac";
	private static final String KEY_BEACON_UUID = "beaconUUID";
	private static final String KEY_BEACON_TYPE = "beaconType";
	private static final String KEY_BEACON_MAJOR = "beaconMajor";
	private static final String KEY_BEACON_MINOR = "beaconMinor";
	private static final String KEY_BEACON_CREATED_TIME = "beaconCreatedTime";
	private static final String KEY_BEACON_LAST_UPDATE = "beaconLastUpdatedTime";

	// TABLE_GROUP
	private static final String KEY_GROUP_NAME = "groupName";
	private static final String KEY_GROUP_CREATED_TIME = "groupCreatedTime";

	// TABLE_BEACON_GROUP_LINK
	private static final String KEY_BEACON_GROUP_LINK_ID = "linkId";
	private static final String KEY_BEACON_GROUP_LINK_TYPE = "groupType";
	private static final String KEY_BEACON_GROUP_LINK_CREATED_TIME = "groupCreatedTime";
	private static final String KEY_BEACON_GROUP_LINK_PRIORITY = "priority";

	// TABLE PLACES
	private static final String KEY_PLACE_NAME = "placeName";

	// TABLE FACILITIES / TABLE RIDES / TABLE EATERIES / TABLE SHOWS /TABLE
	// SHOPS
	private static final String KEY_ATTRACTION_ID = "attractionId";
	private static final String KEY_NAME = "name";
	private static final String KEY_DESC = "description";
	private static final String KEY_OPEN_TIME = "openTime";
	private static final String KEY_CLOSE_TIME = "closeTime";
	private static final String KEY_DURATION = "duration";
	private static final String KEY_CAPACITY = "capacity";
	private static final String KEY_STATUS = "status";
	// ADD OTHER PARAMETERS IF NEEDED

	/**
	 * These tables have been deprecated from the project as this functionality
	 * will no longer be implemented for the time being, the table, key names
	 * and create table strings will remain in the code for further project (in
	 * case its needed).
	 * 
	 * //QUEUE TABLES private static final String TABLE_QUEUE = "queue"; private
	 * static final String TABLE_VIRTUAL_QUEUE = "virtualQueue"; private static
	 * final String TABLE_USER_VIRTUAL_QUEUE_LINK = "userVirtualQueueLink";
	 * 
	 * // TABLE QUEUE & VIRTUAL QUEUE private static final String
	 * KEY_QUEUE_LENGTH = "queueLength"; private static final String
	 * KEY_QUEUE_ID = "queueId"; private static final String
	 * KEY_VIRTUAL_QUEUE_ID = "virtualQueueId";
	 * 
	 * // TABLE USER VIRTUAL QUEUE LINK private static final String
	 * KEY_LINK_TIME = "linkTime"; private static final String KEY_LINK_ID =
	 * "linkId";
	 * 
	 * :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	 * ::::::::::
	 */

	// TABLE ACHIEVEMENTS
	private static final String KEY_ACHI_ID = "achievementId";
	private static final String KEY_ACHI_NAME = "name";
	private static final String KEY_ACHI_DESC = "description";
	private static final String KEY_ACHI_CONTRIBUTION_POINTS = "contributionPoints";
	private static final String KEY_ACHI_TIME = "achievedTime";
	private static final String KEY_ACHI_ICON = "achievementIcon";

	/*
	 ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	 ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: CREATE COMMANDS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	 ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	 */

	/**
	 * As the user is now stored on the GlobalDatabase class (only one user will
	 * ever be needed per session) this table will is no longer needed (for the
	 * time being), the variables related to this table are left commented as it
	 * may be needed for further implementations of this project.
	 * 
	 * 
	 * private static final String CREATE_TABLE_USER = "CREATE TABLE " +
	 * TABLE_USER + "(" + KEY_USER_ID + " TEXT PRIMARY KEY," +
	 * KEY_USER_BEACON_ID + " INTEGER," + KEY_USER_LOCATION_ID+ " INTEGER," +
	 * KEY_USER_NAME + " TEXT," + KEY_USER_TYPE + " TEXT," +
	 * KEY_USER_MOBILE_PHONE + " INTEGER," + "FOREIGN KEY ("+ KEY_USER_BEACON_ID
	 * + ") " + " REFERENCES " + TABLE_BEACON + "("+ KEY_BEACON_ID + ")," +
	 * " FOREIGN KEY(" + KEY_USER_LOCATION_ID+ ")" + " REFERENCES " +
	 * TABLE_LOCATION + "("+ KEY_USER_LOCATION_ID + ")" + ")";
	 * 
	 * 
	 * CREATE TABLE user( userId TEXT PRIMARY KEY, beaconId INTEGER, locationId
	 * INTEGER, userName TEXT, userType TEXT, mobilePhone TEXT, FOREIGN KEY
	 * beaconId REFERENCES beacon(beaconId), FOREIGN KEY locationId REFERENCES
	 * location(locationId))
	 */

	private static String CREATE_TABLE_LOCATION = "CREATE TABLE "
			+ TABLE_LOCATION + "(" + KEY_LOCATION_ID + " INTEGER PRIMARY KEY,"
			+ KEY_LOC_LAT + " REAL," + KEY_LOC_LONG + " REAL," + KEY_LOC_ALT
			+ " REAL," + KEY_LOC_ACCURACY + " INTEGER" + ")";
	/*
	 * CREATE TABLE location( locationId INTEGER PRIMARY KEY, locationLat REAL,
	 * locationLong REAL, locationAlt REAL, locationaAccuracy INTEGER)
	 */

	private static String CREATE_TABLE_BEACON_LOC_LINK = "CREATE TABLE "
			+ TABLE_BEACON_LOC_LINK + "(" + KEY_BEACON_LOC_ID
			+ " INTEGER PRIMARY KEY," + KEY_BEACON_LOC_LINK_BEACON
			+ " INTEGER," + KEY_LOCATION_ID + " INTEGER,"
			+ KEY_BEACON_LOC_OBJECT_ID + " INTEGER," + "FOREIGN KEY ("
			+ KEY_BEACON_LOC_LINK_BEACON + ")" + " REFERENCES " + TABLE_BEACON
			+ "(" + KEY_BEACON_ID + ")," + "FOREIGN KEY (" + KEY_LOCATION_ID
			+ ")" + " REFERENCES " + TABLE_LOCATION + "("
			+ KEY_USER_LOCATION_ID + ")" + ")";
	/*
	 * CREATE TABLE beaconLocationLink( linkId INTEGER PRIMARY KEY, beaconId
	 * INTEGER FOREIGN KEY (beaconId) REFERENCES beacon(beaconId), locationId
	 * INTEGER FOREIGN KEY (locationId) REFERENCES location (locationId),
	 * objectId INTEGER)
	 */

	private static final String CREATE_TABLE_BEACON = "CREATE TABLE "
			+ TABLE_BEACON + "(" + KEY_BEACON_ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_BEACON_MAC + " TEXT,"
			+ KEY_BEACON_UUID + " TEXT," + KEY_BEACON_TYPE + " TEXT,"
			+ KEY_BEACON_MAJOR + " INTEGER," + KEY_BEACON_MINOR + " INTEGER,"
			+ KEY_BEACON_CREATED_TIME + " DATETIME," + KEY_BEACON_LAST_UPDATE
			+ " DATETIME" + ")";
	/*
	 * CREATE TABLE beacon( beaconId INTEGER PRIMARY KEY AUTOINCREMENT, mac
	 * TEXT, uuid TEXT, type TEXT, major INTEGER, minor INTEGER,
	 * beaconCreatedTime DATETIME, beaconLastUpdatedTime DATETIME)
	 */

	private static final String CREATE_TABLE_GROUP = "CREATE TABLE "
			+ TABLE_GROUP + " (" + KEY_GROUP_ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_GROUP_NAME + " TEXT,"
			+ KEY_GROUP_CREATED_TIME + " DATETIME" + ")";
	/*
	 * CREATE TABLE group( groupId INTEGER PRIMARY KEY AUTOINCREMENT, groupName
	 * TEXT, groupCreatedTime DATETIME)
	 */

	private static final String CREATE_TABLE_BEACON_GROUP_LINK = "CREATE TABLE "
			+ TABLE_BEACON_GROUP_LINK
			+ "("
			+ KEY_BEACON_GROUP_LINK_ID
			+ " INTEGER PRIMARY KEY,"
			+ KEY_GROUP_ID
			+ " INTEGER,"
			+ KEY_BEACON_ID
			+ " INTEGER,"
			+ KEY_BEACON_GROUP_LINK_TYPE
			+ " TEXT,"
			+ KEY_BEACON_GROUP_LINK_CREATED_TIME
			+ " DATETIME,"
			+ KEY_BEACON_GROUP_LINK_PRIORITY
			+ " INTEGER,"
			+ "FOREIGN KEY ("
			+ KEY_GROUP_ID
			+ ") "
			+ "REFERENCES "
			+ TABLE_GROUP
			+ " ("
			+ KEY_GROUP_ID
			+ "),"
			+ "FOREIGN KEY ("
			+ KEY_BEACON_ID
			+ ") "
			+ "REFERENCES " + TABLE_BEACON + " (" + KEY_BEACON_ID + ")" + ")";

	/*
	 * CREATE TABLE beaconGroupLink( linkId INTEGER PRIMARY KEY, groupId
	 * INTEGER, beaconId INTEGER, groupType TEXT, groupCreatedTime DATETIME,
	 * priority INTEGER, FOREIGN KEY (groupId) REFERENCES group (groupId),
	 * FOREIGN KEY (beaconId) REFERENCES beacon(beaconId))
	 */

	public static final String CREATE_TABLE_PLACES = "CREATE TABLE "
			+ TABLE_PLACES + "(" + KEY_PLACES_ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_PLACE_NAME + " TEXT,"
			+ KEY_LOCATION_ID + " INTEGER," + "FOREIGN KEY (" + KEY_LOCATION_ID
			+ ") " + "REFERENCES " + TABLE_LOCATION + "(" + KEY_LOCATION_ID
			+ ")" + ")";

	/*
	 * CREATE TABLE places( placeId INTEGER PRIMARY KEY AUTOINCREMENT, placeNAME
	 * TEXT, locationId INTEGER, FOREIGN KEY (locationId) REFERENCES
	 * location(locationId))
	 */

	public static final String CREATE_TABLE_FACILITIES = "CREATE TABLE "
			+ TABLE_FACILITIES + "(" + KEY_ATTRACTION_ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_NAME + " TEXT,"
			+ KEY_DESC + " TEXT," + KEY_OPEN_TIME + " TEXT," + KEY_CLOSE_TIME
			+ " TEXT," + KEY_PLACES_ID + " INTEGER," + "FOREIGN KEY ("
			+ KEY_PLACES_ID + ")" + "REFERENCES " + TABLE_PLACES + "("
			+ KEY_PLACES_ID + ")" + ")";

	/*
	 * CREATE TABLE facilities( attractionId INTEGER PRIMARY KEY AUTOINCREMENT,
	 * name TEXT, description TEXT, openTime TEXT, closeTime TEXT, placesId
	 * INTEGER, FOREIGN KEY (placesId) REFERENCES places(placesId))
	 */

	public static final String CREATE_TABLE_EATERIES = "CREATE TABLE "
			+ TABLE_EATERIES + "(" + KEY_ATTRACTION_ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_NAME + " TEXT,"
			+ KEY_DESC + " TEXT," + KEY_OPEN_TIME + " TEXT," + KEY_CLOSE_TIME
			+ " TEXT," + KEY_PLACES_ID + " INTEGER," + "FOREIGN KEY ("
			+ KEY_PLACES_ID + ")" + "REFERENCES " + TABLE_PLACES + "("
			+ KEY_PLACES_ID + ")" + ")";

	/*
	 * CREATE TABLE eateries( attractionId INTEGER PRIMARY KEY AUTOINCREMENT,
	 * name TEXT, description TEXT, openTime TEXT, closeTime TEXT, placesId
	 * INTEGER, FOREIGN KEY (placesId) REFERENCES places(placesId))
	 */

	public static final String CREATE_TABLE_SHOWS = "CREATE TABLE "
			+ TABLE_SHOWS + "(" + KEY_ATTRACTION_ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_NAME + " TEXT,"
			+ KEY_DESC + " TEXT," + KEY_OPEN_TIME + " TEXT," + KEY_CLOSE_TIME
			+ " TEXT," + KEY_PLACES_ID + " INTEGER," + "FOREIGN KEY ("
			+ KEY_PLACES_ID + ")" + "REFERENCES " + TABLE_PLACES + "("
			+ KEY_PLACES_ID + ")" + ")";

	/*
	 * CREATE TABLE shows( attractionId INTEGER PRIMARY KEY AUTOINCREMENT, name
	 * TEXT, description TEXT, openTime TEXT, closeTime TEXT, placesId INTEGER,
	 * FOREIGN KEY (placesId) REFERENCES places(placesId))
	 */

	public static final String CREATE_TABLE_RIDES = "CREATE TABLE "
			+ TABLE_RIDES + "(" + KEY_ATTRACTION_ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_NAME + " TEXT,"
			+ KEY_DESC + " TEXT," + KEY_OPEN_TIME + " TEXT," + KEY_CLOSE_TIME
			+ " TEXT," + KEY_PLACES_ID + " INTEGER," + KEY_DURATION + " TEXT,"
			+ KEY_CAPACITY + " INTEGER," + KEY_STATUS + " TEXT,"
			+ "FOREIGN KEY (" + KEY_PLACES_ID + ")" + "REFERENCES "
			+ TABLE_PLACES + "(" + KEY_PLACES_ID + ")" + ")";

	/*
	 * CREATE TABLE rides( attractionId INTEGER PRIMARY KEY AUTOINCREMENT, name
	 * TEXT, description TEXT, openTime TEXT, closeTime TEXT, placesId INTEGER,
	 * duration TEXT, capacity INTEGER, status TEXT, FOREIGN KEY (placesId)
	 * REFERENCES places(placesId))
	 */

	public static final String CREATE_TABLE_SHOPS = "CREATE TABLE "
			+ TABLE_SHOPS + "(" + KEY_ATTRACTION_ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_NAME + " TEXT,"
			+ KEY_DESC + " TEXT," + KEY_OPEN_TIME + " TEXT," + KEY_CLOSE_TIME
			+ " TEXT," + KEY_PLACES_ID + " INTEGER," + "FOREIGN KEY ("
			+ KEY_PLACES_ID + ")" + "REFERENCES " + TABLE_PLACES + "("
			+ KEY_PLACES_ID + ")" + ")";

	/*
	 * CREATE TABLE shops( attractionId INTEGER PRIMARY KEY AUTOINCREMENT, name
	 * TEXT, description TEXT, openTime TEXT, closeTime TEXT, placesId INTEGER,
	 * FOREIGN KEY (placesId) REFERENCES places(placesId))
	 */

	/**
	 * These tables have been deprecated from the project as this functionality
	 * will no longer be implemented for the time being, the table, key names
	 * and create table strings will remain in the code for further project (in
	 * case its needed).
	 * 
	 * public static final String CREATE_TABLE_QUEUE = "CREATE TABLE " +
	 * TABLE_QUEUE + "(" + KEY_QUEUE_ID+ " INTEGER PRIMARY KEY AUTOINCREMENT," +
	 * KEY_QUEUE_LENGTH+ " INTEGER," + KEY_ATTRACTION_ID + " INTEGER," +
	 * "FOREIGN KEY ("+ KEY_ATTRACTION_ID + ")" + "REFERENCES " + TABLE_RIDES +
	 * "("+ KEY_ATTRACTION_ID + ")" + ")";
	 * 
	 * 
	 * public static final String CREATE_TABLE_VIRTUAL_QUEUE = "CREATE TABLE " +
	 * TABLE_VIRTUAL_QUEUE + "(" + KEY_VIRTUAL_QUEUE_ID+
	 * " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_QUEUE_LENGTH+ " INTEGER," +
	 * KEY_ATTRACTION_ID + " INTEGER," + "FOREIGN KEY ("+KEY_ATTRACTION_ID+")" +
	 * "REFERENCES "+TABLE_RIDES+"("+KEY_ATTRACTION_ID+")"+")";
	 * 
	 * 
	 * public static final String CREATE_TABLE_USER_VIRTUAL_QUEUE_LINK =
	 * "CREATE TABLE " + TABLE_USER_VIRTUAL_QUEUE_LINK+ "(" + KEY_LINK_ID+
	 * " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_VIRTUAL_QUEUE_ID+ " INTEGER,"
	 * + KEY_USER_ID+ " INTEGER," + KEY_LINK_TIME+ " DATETIME," +
	 * "FOREIGN KEY ("+ KEY_VIRTUAL_QUEUE_ID+ ")" + "REFERENCES "+
	 * TABLE_VIRTUAL_QUEUE+ "("+ KEY_VIRTUAL_QUEUE_ID+ ")," + "FOREIGN KEY ("+
	 * KEY_USER_ID+ ")" + "REFERENCES "+ TABLE_USER + "(" + KEY_USER_ID + ")" +
	 * ")";
	 * 
	 * :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	 * ::::::::::
	 */

	public static final String CREATE_TABLE_ACHIEVEMENTS = "CREATE TABLE "
			+ TABLE_ACHIEVEMENTS + "(" + KEY_ACHI_ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_ACHI_NAME + " TEXT,"
			+ KEY_ACHI_DESC + " TEXT," + KEY_ACHI_CONTRIBUTION_POINTS
			+ " INTEGER," + KEY_ACHI_TIME + " DATETIME," + KEY_ACHI_ICON
			+ " TEXT" + ")";

	/*
	 * CREATE TABLE userVirtualQueueLink( linkId INTEGER PRIMARY KEY
	 * AUTOINCREMENT, virtualQueueId INTEGER, userID INTEGER, linkTime DATETIME,
	 * achievementIcon TEXT, FOREIGN KEY (virtualQueueId) REFERENCES
	 * virtualQueue(virtualQueueId) FOREIGN KEY (userId) REFERENCES
	 * user(userId);
	 */

	/*
	 ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	 ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: DATABASE METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	 ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	 */

	// CONSTRUCTOR
	public DatabaseHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	// CREATE TABLES ON CREATE
	@Override
	public void onCreate(SQLiteDatabase db) {

		db.execSQL(CREATE_TABLE_BEACON);
		db.execSQL(CREATE_TABLE_GROUP);
		db.execSQL(CREATE_TABLE_BEACON_GROUP_LINK);
		db.execSQL(CREATE_TABLE_LOCATION);
		db.execSQL(CREATE_TABLE_BEACON_LOC_LINK);
		db.execSQL(CREATE_TABLE_PLACES);
		db.execSQL(CREATE_TABLE_FACILITIES);
		db.execSQL(CREATE_TABLE_EATERIES);
		db.execSQL(CREATE_TABLE_SHOWS);
		db.execSQL(CREATE_TABLE_RIDES);
		db.execSQL(CREATE_TABLE_SHOPS);
		db.execSQL(CREATE_TABLE_ACHIEVEMENTS);
		Log.d(LOG, "Database Created");
	}

	// ONUPGRADE
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

		// Drop Older table if existed
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_BEACON);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_GROUP);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_BEACON_GROUP_LINK);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_LOCATION);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_BEACON_LOC_LINK);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_PLACES);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_FACILITIES);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_EATERIES);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_SHOWS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_RIDES);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_SHOPS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_ACHIEVEMENTS);

		// Create tables
		onCreate(db);
	}

	public void dropTables() {

		SQLiteDatabase db = this.getWritableDatabase();
		// Drop Older table if existed
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_BEACON);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_GROUP);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_BEACON_GROUP_LINK);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_LOCATION);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_BEACON_LOC_LINK);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_PLACES);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_FACILITIES);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_EATERIES);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_SHOWS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_RIDES);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_SHOPS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_ACHIEVEMENTS);
	}

	// RESET DATABASE
	public void resetTables() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_BEACON, null, null);
		db.delete(TABLE_GROUP, null, null);
		db.delete(TABLE_BEACON_GROUP_LINK, null, null);
		db.delete(TABLE_LOCATION, null, null);
		db.delete(TABLE_BEACON_LOC_LINK, null, null);
		db.delete(TABLE_PLACES, null, null);
		db.delete(TABLE_FACILITIES, null, null);
		db.delete(TABLE_EATERIES, null, null);
		db.delete(TABLE_SHOWS, null, null);
		db.delete(TABLE_RIDES, null, null);
		db.delete(TABLE_SHOPS, null, null);
		db.delete(TABLE_ACHIEVEMENTS, null, null);
		db.close();

		Log.d(LOG, "Reseted tables");
	}

	public void upgrade(int old, int n) {

		SQLiteDatabase db = this.getWritableDatabase();
		this.onUpgrade(db, old, n);
	}

	/*
	 ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	 ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: CLASS FUNCTIONS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	 ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	 */

	/*
	 ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	 ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: BEACON FUNCTIONS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	 ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	 */
	// ADDS BEACON TO DATABASE
	public void addBeaconToDB(Beacon beacon) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();

		values.put(KEY_BEACON_MAC, beacon.getBeaconMac());
		values.put(KEY_BEACON_UUID, beacon.getBeaconUUID());
		values.put(KEY_BEACON_MAJOR, beacon.getBeaconMajor());
		values.put(KEY_BEACON_MINOR, beacon.getBeaconMinor());

		// Insert Row
		db.insert(TABLE_BEACON, null, values);
	}

	// "SELECT * FROM TABLE_BEACON;
	public List<Beacon> getAllBeacons() {
		List<Beacon> beaconList = new ArrayList<Beacon>();
		String selectQuery = "SELECT  * FROM " + TABLE_BEACON;

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (c.moveToFirst()) {
			do {
				Beacon bc = new Beacon();
				bc.setBeaconMac(c.getString(c.getColumnIndex(KEY_BEACON_MAC)));
				bc.setBeaconUUID(c.getString(c.getColumnIndex(KEY_BEACON_UUID)));
				bc.setBeaconMajor(c.getInt((c.getColumnIndex(KEY_BEACON_MAJOR))));
				bc.setBeaconMinor(c.getInt((c.getColumnIndex(KEY_BEACON_MINOR))));

				// adding to beacon list
				beaconList.add(bc);
			} while (c.moveToNext());
		}
		return beaconList;
	}

	// SELECT ID FROM BEACON WHERE UUID = X;
	public Beacon getBeaconFromUUID(String uuid) {
		SQLiteDatabase db = this.getReadableDatabase();

		String selectQuery = "SELECT  * FROM " + TABLE_BEACON + " WHERE "
				+ KEY_BEACON_UUID + "='" + uuid + "'";

		Cursor c = db.rawQuery(selectQuery, null);
		if (c != null)
			c.moveToFirst();

		Beacon bc = new Beacon();
		bc.setBeaconMac(c.getString(c.getColumnIndex(KEY_BEACON_MAC)));
		bc.setBeaconUUID(c.getString(c.getColumnIndex(KEY_BEACON_UUID)));
		bc.setBeaconMajor(c.getInt(c.getColumnIndex(KEY_BEACON_MAJOR)));
		bc.setBeaconMinor(c.getInt(c.getColumnIndex(KEY_BEACON_MINOR)));
		bc.setBeaconId(c.getInt(c.getColumnIndex(KEY_BEACON_ID)));

		return bc;
	}

	// VERIFIES EXISTENCE OF BEACON IN DATABASE BY UUID

	public boolean checkBeaconFromUUID(String uuid) {
		SQLiteDatabase db = this.getReadableDatabase();

		String selectQuery = "SELECT  * FROM " + TABLE_BEACON + " WHERE "
				+ KEY_BEACON_UUID + "=" + uuid;

		Log.e(LOG, selectQuery);

		Cursor c = db.rawQuery(selectQuery, null);
		if (c != null) {
			Log.i(LOG, "Entry found");
			return true;
		} else {
			Log.i(LOG, "Entry NOT found");
			return false;
		}
	}

	/*
	 ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	 ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: BEACON - GROUP FUNCTIONS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	 ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	 */

	// / NEEDS TO BE CHECKED AND IMPLEMENTED CORRECTLY - links a beacon to a
	// group
	public void linkBeaconToGroup(Beacon b, Group g, DatabaseHandler d) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();

		Beacon beacon = d.getBeaconFromUUID(b.getBeaconUUID());

		values.put(KEY_GROUP_ID, d.getGroupID(g.getGroupName()));
		values.put(KEY_BEACON_ID, beacon.getBeaconId());

		db.insert(TABLE_BEACON_GROUP_LINK, null, values);

	}

	public void linkBeaconToGroup(int groupId, int beaconId) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_GROUP_ID, groupId);
		values.put(KEY_BEACON_ID, beaconId);

		db.insert(TABLE_BEACON_GROUP_LINK, null, values);
	}

	// FOR TESTING CORRECTNES OF LINKING TABLE
	public void getLinkedBeacons() {
		List<Group> groupList = new ArrayList<Group>();
		List<Beacon> beaconList = new ArrayList<Beacon>();
		String selectQuery = "SELECT  * FROM " + TABLE_BEACON_GROUP_LINK;

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (c.moveToFirst()) {
			do {
				Group group = new Group();
				Beacon beacon = new Beacon();
				group.setId(c.getInt(c.getColumnIndex(KEY_GROUP_ID)));
				beacon.setBeaconId(c.getInt(c.getColumnIndex(KEY_BEACON_ID)));

				String beaconQuery = "SELECT * FROM " + TABLE_BEACON
						+ " WHERE " + KEY_BEACON_ID + "='"
						+ beacon.getBeaconId() + "'";
				String groupQuery = "SELECT * FROM " + TABLE_GROUP + " WHERE "
						+ KEY_GROUP_ID + "='" + group.getId() + "'";

				Cursor gCursor = db.rawQuery(groupQuery, null);
				if (gCursor.moveToFirst()) {
					do {
						group.setGroupName(gCursor.getString(gCursor
								.getColumnIndex(KEY_GROUP_NAME)));
						group.setGroupCreatedTime(gCursor.getString(gCursor
								.getColumnIndex(KEY_GROUP_CREATED_TIME)));
						groupList.add(group);
					} while (gCursor.moveToNext());
				}

				Cursor bCursor = db.rawQuery(beaconQuery, null);
				if (bCursor.moveToFirst()) {
					do {
						beacon.setBeaconUUID(bCursor.getString(bCursor
								.getColumnIndex(KEY_BEACON_UUID)));
						beacon.setBeaconMajor(bCursor.getInt(bCursor
								.getColumnIndex(KEY_BEACON_MAJOR)));
						beacon.setBeaconMinor(bCursor.getInt(bCursor
								.getColumnIndex(KEY_BEACON_MINOR)));
						beaconList.add(beacon);

					} while (bCursor.moveToNext());
				}
			} while (c.moveToNext());
		}

		for (int i = 0; i < groupList.size(); i++) {
			Log.e("GROUP", groupList.get(i).getGroupName());
			Log.e("BEACON", beaconList.get(i).getBeaconUUID());
		}
	}

	// GET ALL BEACONS FROM A GROUP ID
	public List<Beacon> getAllBeaconsOfGroup(int groupID) {

		List<Beacon> beaconList = new ArrayList<Beacon>();
		List<Integer> beaconId = new ArrayList<Integer>();
		String query = "SELECT " + KEY_BEACON_ID + " FROM "
				+ TABLE_BEACON_GROUP_LINK + " WHERE " + KEY_GROUP_ID + "='"
				+ groupID + "'";

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.rawQuery(query, null);

		if (c.moveToFirst()) {
			do {
				beaconId.add(c.getInt(c.getColumnIndex(KEY_BEACON_ID)));

			} while (c.moveToNext());
		}

		for (int i = 0; i < beaconId.size(); i++) {

			String selectQuery = "SELECT * FROM " + TABLE_BEACON + " WHERE "
					+ KEY_BEACON_ID + "='" + beaconId.get(i) + "'";

			Cursor cu = db.rawQuery(selectQuery, null);
			if (cu.moveToFirst()) {
				do {
					Beacon beacon = new Beacon();
					beacon.setBeaconUUID(cu.getString(cu
							.getColumnIndex(KEY_BEACON_UUID)));
					beacon.setBeaconMajor(cu.getInt(cu
							.getColumnIndex(KEY_BEACON_MAJOR)));
					beacon.setBeaconMinor(cu.getInt(cu
							.getColumnIndex(KEY_BEACON_MINOR)));
					beaconList.add(beacon);
				} while (cu.moveToNext());
			}
		}

		return beaconList;
	}

	public void dropGroupTable() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_GROUP);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_BEACON);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_BEACON_GROUP_LINK);
		db.execSQL(CREATE_TABLE_GROUP);
		db.execSQL(CREATE_TABLE_BEACON);
		db.execSQL(CREATE_TABLE_BEACON_GROUP_LINK);
	}

	/*
	 ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	 :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: GROUP FUNCTIONS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	 ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	 */

	// ADD GROUP TO DATABASE
	public void addGroup(Group group) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();

		values.put(KEY_GROUP_NAME, group.getGroupName());
		values.put(KEY_GROUP_CREATED_TIME, group.getGroupCreatedTime());

		db.insert(TABLE_GROUP, null, values);
	}

	// "SELECT * FROM TABLE_GROUP;
	public List<Group> getAllGroups() {
		List<Group> groupList = new ArrayList<Group>();
		String selectQuery = "SELECT  * FROM " + TABLE_GROUP;

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (c.moveToFirst()) {
			do {
				Group group = new Group();
				group.setGroupName(c.getString(c.getColumnIndex(KEY_GROUP_NAME)));
				group.setGroupCreatedTime(c.getString(c
						.getColumnIndex(KEY_GROUP_CREATED_TIME)));
				group.setId(c.getInt(c.getColumnIndex(KEY_GROUP_ID)));

				// adding to beacon list
				groupList.add(group);
			} while (c.moveToNext());
		}
		return groupList;
	}

	// "SELECT * FROM TABLE_GROUP WHERE groupName;
	public int getGroupID(String name) {
		String selectQuery = "SELECT * FROM " + TABLE_GROUP + " WHERE "
				+ KEY_GROUP_NAME + "= '" + name + "'";
		List<Group> groupList = new ArrayList<Group>();
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (c.moveToFirst()) {
			do {
				Group group = new Group();
				group.setGroupName(c.getString(c.getColumnIndex(KEY_GROUP_NAME)));
				group.setGroupCreatedTime(c.getString(c
						.getColumnIndex(KEY_GROUP_CREATED_TIME)));
				group.setId(c.getInt(c.getColumnIndex(KEY_GROUP_ID)));

				// adding to beacon list
				groupList.add(group);
			} while (c.moveToNext());
		}
		return groupList.get(0).getId();
	}

	/*
	 ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	 ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: STATIC LOCATION FUNCTIONS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	 ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	 */

	public void addFacility(StaticLocation sl) {

		SQLiteDatabase db = this.getWritableDatabase();

		// Creates a new entry on TABLE_LOCATION
		ContentValues values = new ContentValues();
		values.put(KEY_LOC_LAT, sl.getLatitude());
		values.put(KEY_LOC_LONG, sl.getLongitude());

		int index = (int) db.insert(TABLE_LOCATION, null, values); // returns id
																	// of the
																	// inserted
																	// row

		// Creates a new entry on TABLE_PLACE
		values = new ContentValues();

		values.put(KEY_PLACE_NAME, sl.getName());
		values.put(KEY_LOCATION_ID, index);

		index = (int) db.insert(TABLE_PLACES, null, values); // returns id of
																// the inserted
																// row

		// Inserts a new entry on TABLE_FACILITY
		values = new ContentValues();

		values.put(KEY_NAME, sl.getName());
		values.put(KEY_DESC, sl.getDetails());
		values.put(KEY_OPEN_TIME, sl.getOpenTime());
		values.put(KEY_CLOSE_TIME, sl.getCloseTime());
		values.put(KEY_PLACES_ID, index);

		db.insert(TABLE_FACILITIES, null, values);
	}

	public void addShow(StaticLocation sl) {

		SQLiteDatabase db = this.getWritableDatabase();

		// Creates a new entry on TABLE_LOCATION
		ContentValues values = new ContentValues();
		values.put(KEY_LOC_LAT, sl.getLatitude());
		values.put(KEY_LOC_LONG, sl.getLongitude());

		int index = (int) db.insert(TABLE_LOCATION, null, values); // returns id
																	// of
																	// inserted
																	// row

		// Creates a new entry on TABLE_PLACE
		values = new ContentValues();

		values.put(KEY_PLACE_NAME, sl.getName());
		values.put(KEY_LOCATION_ID, index);

		index = (int) db.insert(TABLE_PLACES, null, values); // returns id of
																// inserted row

		// Inserts a new entry on TABLE_SHOWS
		values = new ContentValues();

		values.put(KEY_NAME, sl.getName());
		values.put(KEY_DESC, sl.getDetails());
		values.put(KEY_OPEN_TIME, sl.getOpenTime());
		values.put(KEY_CLOSE_TIME, sl.getCloseTime());
		values.put(KEY_PLACES_ID, index);

		db.insert(TABLE_SHOWS, null, values);

	}

	public void addEatery(StaticLocation sl) {
		SQLiteDatabase db = this.getWritableDatabase();

		// Creates a new entry on TABLE_LOCATION
		ContentValues values = new ContentValues();
		values.put(KEY_LOC_LAT, sl.getLatitude());
		values.put(KEY_LOC_LONG, sl.getLongitude());

		int index = (int) db.insert(TABLE_LOCATION, null, values); // returns id
																	// of
																	// inserted
																	// row

		// Creates a new entry on TABLE_PLACE
		values = new ContentValues();

		values.put(KEY_PLACE_NAME, sl.getName());
		values.put(KEY_LOCATION_ID, index);

		index = (int) db.insert(TABLE_PLACES, null, values); // returns id of
																// inserted row

		// Inserts a new entry on TABLE_EATERIES
		values = new ContentValues();

		values.put(KEY_NAME, sl.getName());
		values.put(KEY_DESC, sl.getDetails());
		values.put(KEY_OPEN_TIME, sl.getOpenTime());
		values.put(KEY_CLOSE_TIME, sl.getCloseTime());
		values.put(KEY_PLACES_ID, index);

		db.insert(TABLE_EATERIES, null, values);
	}

	// ADDS RIDE TO DATABASE
	public void addRides(StaticLocation sl) {
		SQLiteDatabase db = this.getWritableDatabase();

		// Creates a new entry on TABLE_LOCATION
		ContentValues values = new ContentValues();
		values.put(KEY_LOC_LAT, sl.getLatitude());
		values.put(KEY_LOC_LONG, sl.getLongitude());

		int index = (int) db.insert(TABLE_LOCATION, null, values); // RETURNS ID
																	// OF
																	// INSERTED
																	// ROW

		// Creates a new entry on TABLE_PLACE
		values = new ContentValues();

		values.put(KEY_PLACE_NAME, sl.getName());
		values.put(KEY_LOCATION_ID, index);

		index = (int) db.insert(TABLE_PLACES, null, values); // RETURNS ID OF
																// INSERTED ROW

		// Inserts a new entry on TABLE_RIDES
		values = new ContentValues();

		values.put(KEY_NAME, sl.getName());
		values.put(KEY_DESC, sl.getDetails());
		values.put(KEY_OPEN_TIME, sl.getOpenTime());
		values.put(KEY_CLOSE_TIME, sl.getCloseTime());
		values.put(KEY_PLACES_ID, index);

		db.insert(TABLE_RIDES, null, values);
	}

	public void addShops(StaticLocation sl) {
		SQLiteDatabase db = this.getWritableDatabase();

		// Creates a new entry on TABLE_LOCATION
		ContentValues values = new ContentValues();
		values.put(KEY_LOC_LAT, sl.getLatitude());
		values.put(KEY_LOC_LONG, sl.getLongitude());

		int index = (int) db.insert(TABLE_LOCATION, null, values); // returns id
																	// of the
																	// inserted
																	// row

		// Creates a new entry on TABLE_PLACE
		values = new ContentValues();

		values.put(KEY_PLACE_NAME, sl.getName());
		values.put(KEY_LOCATION_ID, index);

		index = (int) db.insert(TABLE_PLACES, null, values); // returns id of
																// the inserted
																// row

		// Inserts a new entry on TABLE_SHOPS
		values = new ContentValues();

		values.put(KEY_NAME, sl.getName());
		values.put(KEY_DESC, sl.getDetails());
		values.put(KEY_OPEN_TIME, sl.getOpenTime());
		values.put(KEY_CLOSE_TIME, sl.getCloseTime());
		values.put(KEY_PLACES_ID, index);

		db.insert(TABLE_SHOPS, null, values);
	}

	// QUERY ALL RIDES
	public List<StaticLocation> getAllRides() {
		List<StaticLocation> attractionList = new ArrayList<StaticLocation>();
		String selectQuery = "SELECT * FROM " + TABLE_RIDES;
		int placeId = 0;

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (c.moveToFirst()) {
			do { // set values of ride table
				StaticLocation sl = new StaticLocation();
				sl.setName(c.getString(c.getColumnIndex(KEY_NAME)));
				sl.setOpenTime(c.getString(c.getColumnIndex(KEY_OPEN_TIME)));
				sl.setCloseTime(c.getString(c.getColumnIndex(KEY_CLOSE_TIME)));
				sl.setDetails(c.getString(c.getColumnIndex(KEY_DESC)));
				placeId = c.getInt(c.getColumnIndex(KEY_PLACES_ID));

				// set values of object by linking table to location
				String query = "SELECT * FROM location WHERE locationId=("
						+ "SELECT locationId FROM places WHERE placeId='"
						+ placeId + "')";

				Cursor cursor = db.rawQuery(query, null);
				if (cursor.moveToFirst()) {
					sl.setLatitude(cursor.getDouble(cursor
							.getColumnIndex(KEY_LOC_LAT)));
					sl.setLongitude(cursor.getDouble(cursor
							.getColumnIndex(KEY_LOC_LONG)));
				}

				// adding to attraction list
				attractionList.add(sl);
			} while (c.moveToNext());
		}
		return attractionList;
	}

	// QUERY ALL SHOPS

	public List<StaticLocation> getAllShops() {
		List<StaticLocation> attractionList = new ArrayList<StaticLocation>();
		String selectQuery = "SELECT  * FROM " + TABLE_SHOPS;
		int placeId;

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (c.moveToFirst()) {
			do {
				StaticLocation sl = new StaticLocation();
				sl.setName(c.getString(c.getColumnIndex(KEY_NAME)));
				sl.setOpenTime(c.getString(c.getColumnIndex(KEY_OPEN_TIME)));
				sl.setCloseTime(c.getString(c.getColumnIndex(KEY_CLOSE_TIME)));
				sl.setDetails(c.getString(c.getColumnIndex(KEY_DESC)));
				placeId = c.getInt(c.getColumnIndex(KEY_PLACES_ID));

				// set values of object by linking table to location
				String query = "SELECT * FROM location WHERE locationId=("
						+ "SELECT locationId FROM places WHERE placeId='"
						+ placeId + "')";

				Cursor cursor = db.rawQuery(query, null);
				if (cursor.moveToFirst()) {
					sl.setLatitude(cursor.getDouble(cursor
							.getColumnIndex(KEY_LOC_LAT)));
					sl.setLongitude(cursor.getDouble(cursor
							.getColumnIndex(KEY_LOC_LONG)));
				}

				// adding to attraction list
				attractionList.add(sl);
			} while (c.moveToNext());
		}
		return attractionList;
	}

	// QUERY ALL EATERIES

	public List<StaticLocation> getAllEateries() {
		List<StaticLocation> attractionList = new ArrayList<StaticLocation>();
		String selectQuery = "SELECT  * FROM " + TABLE_EATERIES;
		int placeId;

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (c.moveToFirst()) {
			do {
				StaticLocation sl = new StaticLocation();
				sl.setName(c.getString(c.getColumnIndex(KEY_NAME)));
				sl.setOpenTime(c.getString(c.getColumnIndex(KEY_OPEN_TIME)));
				sl.setCloseTime(c.getString(c.getColumnIndex(KEY_CLOSE_TIME)));
				sl.setDetails(c.getString(c.getColumnIndex(KEY_DESC)));
				placeId = c.getInt(c.getColumnIndex(KEY_PLACES_ID));

				// set values of object by linking table to location
				String query = "SELECT * FROM location WHERE locationId=("
						+ "SELECT locationId FROM places WHERE placeId='"
						+ placeId + "')";

				Cursor cursor = db.rawQuery(query, null);
				if (cursor.moveToFirst()) {
					sl.setLatitude(cursor.getDouble(cursor
							.getColumnIndex(KEY_LOC_LAT)));
					sl.setLongitude(cursor.getDouble(cursor
							.getColumnIndex(KEY_LOC_LONG)));
				}

				// adding to attraction list
				attractionList.add(sl);
			} while (c.moveToNext());
		}
		return attractionList;
	}

	public List<StaticLocation> getAllSl(String type) {

		List<StaticLocation> attractionList = new ArrayList<StaticLocation>();
		SQLiteDatabase db = this.getReadableDatabase();
		String selectQuery = "";
		int placeId;

		if (type.equalsIgnoreCase("shows")) {
			selectQuery = "SELECT  * FROM " + TABLE_SHOWS;
		} else if (type.equalsIgnoreCase("eateries")) {
			selectQuery = "SELECT  * FROM " + TABLE_EATERIES;
		} else if (type.equalsIgnoreCase("rides")) {
			selectQuery = "SELECT  * FROM " + TABLE_RIDES;
		} else if (type.equalsIgnoreCase("facilities")) {
			selectQuery = "SELECT  * FROM " + TABLE_FACILITIES;
		} else if (type.equalsIgnoreCase("shops")) {
			selectQuery = "SELECT  * FROM " + TABLE_SHOPS;
		} else {
			Log.e("DatabaseHandler/getAllSl METHOD",
					"Incorrect parameter, must be either shows, eateries, rides, facilities or shops");
			return null;
		}
		Cursor c = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (c.moveToFirst()) {
			do {
				StaticLocation sl = new StaticLocation();
				sl.setName(c.getString(c.getColumnIndex(KEY_NAME)));
				sl.setOpenTime(c.getString(c.getColumnIndex(KEY_OPEN_TIME)));
				sl.setCloseTime(c.getString(c.getColumnIndex(KEY_CLOSE_TIME)));
				sl.setDetails(c.getString(c.getColumnIndex(KEY_DESC)));
				placeId = c.getInt(c.getColumnIndex(KEY_PLACES_ID));

				// set values of object by linking table to location
				String query = "SELECT * FROM location WHERE locationId=("
						+ "SELECT locationId FROM places WHERE placeId='"
						+ placeId + "')";

				Cursor cursor = db.rawQuery(query, null);
				if (cursor.moveToFirst()) {
					sl.setLatitude(cursor.getDouble(cursor
							.getColumnIndex(KEY_LOC_LAT)));
					sl.setLongitude(cursor.getDouble(cursor
							.getColumnIndex(KEY_LOC_LONG)));
				}

				// adding to attraction list
				attractionList.add(sl);
			} while (c.moveToNext());
		}
		return attractionList;

	}

	// QUERY ALL FACILITIES
	public List<StaticLocation> getAllFacilities() {
		List<StaticLocation> attractionList = new ArrayList<StaticLocation>();
		String selectQuery = "SELECT  * FROM " + TABLE_FACILITIES;
		int placeId;

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (c.moveToFirst()) {
			do {
				StaticLocation sl = new StaticLocation();
				sl.setName(c.getString(c.getColumnIndex(KEY_NAME)));
				sl.setOpenTime(c.getString(c.getColumnIndex(KEY_OPEN_TIME)));
				sl.setCloseTime(c.getString(c.getColumnIndex(KEY_CLOSE_TIME)));
				sl.setDetails(c.getString(c.getColumnIndex(KEY_DESC)));
				placeId = c.getInt(c.getColumnIndex(KEY_PLACES_ID));

				// set values of object by linking table to location
				String query = "SELECT * FROM location WHERE locationId=("
						+ "SELECT locationId FROM places WHERE placeId='"
						+ placeId + "')";

				Cursor cursor = db.rawQuery(query, null);
				if (cursor.moveToFirst()) {
					sl.setLatitude(cursor.getDouble(cursor
							.getColumnIndex(KEY_LOC_LAT)));
					sl.setLongitude(cursor.getDouble(cursor
							.getColumnIndex(KEY_LOC_LONG)));
				}

				// adding to attraction list
				attractionList.add(sl);
			} while (c.moveToNext());
		}
		return attractionList;
	}

	// QUERY ALL Shows
	public List<StaticLocation> getAllShows() {
		List<StaticLocation> attractionList = new ArrayList<StaticLocation>();
		String selectQuery = "SELECT  * FROM " + TABLE_SHOWS;
		int placeId;

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (c.moveToFirst()) {
			do {
				StaticLocation sl = new StaticLocation();
				sl.setName(c.getString(c.getColumnIndex(KEY_NAME)));
				sl.setOpenTime(c.getString(c.getColumnIndex(KEY_OPEN_TIME)));
				sl.setCloseTime(c.getString(c.getColumnIndex(KEY_CLOSE_TIME)));
				sl.setDetails(c.getString(c.getColumnIndex(KEY_DESC)));
				placeId = c.getInt(c.getColumnIndex(KEY_PLACES_ID));

				// set values of object by linking table to location
				String query = "SELECT * FROM location WHERE locationId=("
						+ "SELECT locationId FROM places WHERE placeId='"
						+ placeId + "')";

				Cursor cursor = db.rawQuery(query, null);
				if (cursor.moveToFirst()) {
					sl.setLatitude(cursor.getDouble(cursor
							.getColumnIndex(KEY_LOC_LAT)));
					sl.setLongitude(cursor.getDouble(cursor
							.getColumnIndex(KEY_LOC_LONG)));
				}

				// adding to attraction list
				attractionList.add(sl);
			} while (c.moveToNext());
		}
		return attractionList;
	}

	// GET ALL RELEVANT ATTRACTIONS FOR PLANNER;
	public List<StaticLocation> getAllAttractions() {
		List<StaticLocation> attractionList = new ArrayList<StaticLocation>();
		String selectQuery = "SELECT  * FROM " + TABLE_SHOWS;
		int placeId;

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (c.moveToFirst()) {
			do {
				StaticLocation sl = new StaticLocation();
				sl.setName(c.getString(c.getColumnIndex(KEY_NAME)));
				sl.setOpenTime(c.getString(c.getColumnIndex(KEY_OPEN_TIME)));
				sl.setCloseTime(c.getString(c.getColumnIndex(KEY_CLOSE_TIME)));
				sl.setDetails(c.getString(c.getColumnIndex(KEY_DESC)));
				placeId = c.getInt(c.getColumnIndex(KEY_PLACES_ID));

				// set values of object by linking table to location
				String query = "SELECT * FROM location WHERE locationId=("
						+ "SELECT locationId FROM places WHERE placeId='"
						+ placeId + "')";

				Cursor cursor = db.rawQuery(query, null);
				if (cursor.moveToFirst()) {
					sl.setLatitude(cursor.getDouble(cursor
							.getColumnIndex(KEY_LOC_LAT)));
					sl.setLongitude(cursor.getDouble(cursor
							.getColumnIndex(KEY_LOC_LONG)));
				}

				// adding to attraction list
				attractionList.add(sl);
			} while (c.moveToNext());
		}

		selectQuery = "SELECT * FROM " + TABLE_RIDES;
		c = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (c.moveToFirst()) {
			do {
				StaticLocation sl = new StaticLocation();
				sl.setName(c.getString(c.getColumnIndex(KEY_NAME)));
				sl.setOpenTime(c.getString(c.getColumnIndex(KEY_OPEN_TIME)));
				sl.setCloseTime(c.getString(c.getColumnIndex(KEY_CLOSE_TIME)));
				sl.setDetails(c.getString(c.getColumnIndex(KEY_DESC)));
				placeId = c.getInt(c.getColumnIndex(KEY_PLACES_ID));

				// set values of object by linking table to location
				String query = "SELECT * FROM location WHERE locationId=("
						+ "SELECT locationId FROM places WHERE placeId='"
						+ placeId + "')";

				Cursor cursor = db.rawQuery(query, null);
				if (cursor.moveToFirst()) {
					sl.setLatitude(cursor.getDouble(cursor
							.getColumnIndex(KEY_LOC_LAT)));
					sl.setLongitude(cursor.getDouble(cursor
							.getColumnIndex(KEY_LOC_LONG)));
				}

				// adding to attraction list
				attractionList.add(sl);
			} while (c.moveToNext());
		}

		selectQuery = "SELECT * FROM " + TABLE_EATERIES;
		c = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (c.moveToFirst()) {
			do {
				StaticLocation sl = new StaticLocation();
				sl.setName(c.getString(c.getColumnIndex(KEY_NAME)));
				sl.setOpenTime(c.getString(c.getColumnIndex(KEY_OPEN_TIME)));
				sl.setCloseTime(c.getString(c.getColumnIndex(KEY_CLOSE_TIME)));
				sl.setDetails(c.getString(c.getColumnIndex(KEY_DESC)));
				placeId = c.getInt(c.getColumnIndex(KEY_PLACES_ID));

				// set values of object by linking table to location
				String query = "SELECT * FROM location WHERE locationId=("
						+ "SELECT locationId FROM places WHERE placeId='"
						+ placeId + "')";

				Cursor cursor = db.rawQuery(query, null);
				if (cursor.moveToFirst()) {
					sl.setLatitude(cursor.getDouble(cursor
							.getColumnIndex(KEY_LOC_LAT)));
					sl.setLongitude(cursor.getDouble(cursor
							.getColumnIndex(KEY_LOC_LONG)));
				}

				// adding to attraction list
				attractionList.add(sl);
			} while (c.moveToNext());
		}

		selectQuery = "SELECT * FROM " + TABLE_SHOPS;
		c = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (c.moveToFirst()) {
			do {
				StaticLocation sl = new StaticLocation();
				sl.setName(c.getString(c.getColumnIndex(KEY_NAME)));
				sl.setOpenTime(c.getString(c.getColumnIndex(KEY_OPEN_TIME)));
				sl.setCloseTime(c.getString(c.getColumnIndex(KEY_CLOSE_TIME)));
				sl.setDetails(c.getString(c.getColumnIndex(KEY_DESC)));
				placeId = c.getInt(c.getColumnIndex(KEY_PLACES_ID));

				// set values of object by linking table to location
				String query = "SELECT * FROM location WHERE locationId=("
						+ "SELECT locationId FROM places WHERE placeId='"
						+ placeId + "')";

				Cursor cursor = db.rawQuery(query, null);
				if (cursor.moveToFirst()) {
					sl.setLatitude(cursor.getDouble(cursor
							.getColumnIndex(KEY_LOC_LAT)));
					sl.setLongitude(cursor.getDouble(cursor
							.getColumnIndex(KEY_LOC_LONG)));
				}

				// adding to attraction list
				attractionList.add(sl);
			} while (c.moveToNext());
		}

		return attractionList;
	}

	/*
	 ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	 ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: ACHIEVEMENTS FUNCTIONS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	 ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	 */

	// ADD GROUP TO DATABASE
	public void addAchievement(UserAchievement achievement) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();

		values.put(KEY_ACHI_NAME, achievement.getAchievementTitle());
		values.put(KEY_ACHI_DESC, achievement.getAchievementDescription());
		values.put(KEY_ACHI_CONTRIBUTION_POINTS,
				achievement.getAchievementPoints());
		values.put(KEY_ACHI_ICON, achievement.getIconName()); // STRING VALUE OF
																// IMAGE

		db.insert(TABLE_ACHIEVEMENTS, null, values);
	}

	// "SELECT * FROM TABLE_GROUP;
	public List<UserAchievement> getAllAchievements() {
		List<UserAchievement> achievements = new ArrayList<UserAchievement>();
		String selectQuery = "SELECT  * FROM " + TABLE_ACHIEVEMENTS;

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (c.moveToFirst()) {
			do {
				UserAchievement achievement = new UserAchievement();
				achievement.setAchievementTitle(c.getString(c
						.getColumnIndex(KEY_ACHI_NAME)));
				achievement.setAchievementPoints(c.getInt(c
						.getColumnIndex(KEY_ACHI_CONTRIBUTION_POINTS)));
				achievement.setAchiementDescription(c.getString(c
						.getColumnIndex(KEY_ACHI_DESC)));
				// achievement.setAchievementIcon(c.getString(c.getColumnIndex(KEY_ACHI_ICON)));

				// adding to beacon list
				achievements.add(achievement);
			} while (c.moveToNext());
		}
		return achievements;
	}

}
