package com.greenhills.database;

import android.graphics.drawable.Drawable;

import com.google.android.gms.maps.model.LatLng;
import com.greenhills.services.ImageUtils;

public class UserAchievement {
	/* ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: CLASS VARIABLES ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	
	private int uniqueId;
	private Drawable achievementIcon;
	private String achievementTitle;
	private String achievementDescription;
	private int achievementPoints;
	private String timeAchievementWon;
	private String icon;

	
	/* ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: CONSTRUCTORS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	
	public UserAchievement()		//- Default Constructor
	{
		uniqueId 				= 0;							//- Setting to null
		achievementIcon 		= null;							//- Setting to null
		achievementTitle 		= null;							//- Setting to null
		achievementDescription 	= null;							//- Setting to null
		setAchievementPoints(0);								//- Setting to null
		setTimeAchievementWon(null);							//- Setting to null
		icon 					="";							//- Setting to null
		
	}
	public UserAchievement( Drawable achievementIcon, String achievementTitle, String achievementDescription, int achievementPoints, String timeAchievementWon )		//- Default Constructor
	{
		this.uniqueId 				= 0;
		this.achievementIcon 		= achievementIcon;							//- Setting to input
		this.achievementTitle 		= achievementTitle;							//- Setting to input
		this.achievementDescription = achievementDescription;					//- Setting to input
		this.setAchievementPoints(achievementPoints);							//- Setting to input
		this.setTimeAchievementWon(timeAchievementWon);							//- Setting to input
		this.icon 					="";										//- Setting to null
	}
	
	  /* ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: MUTATORS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	  
	  
	  
	  public void setAchievementIcon(byte[] iconData) {
		  achievementIcon = ImageUtils.byteToDrawable(iconData);
	  }
	  
	  //- Wherever you retrieve your data from DB it'll be enough with adding something like that:
	  
	  //- code: user.setPictureData(cursor.getBlob(cursor.getColumnIndexOrThrow(DB_PICTURE_DATA_KEY)));
	  //- http://stackoverflow.com/questions/10831151/how-to-store-and-retrieve-images-in-android-sqlite-database-and-display-them-on
	  //-  For the opposite way (writing the drawable to DB)
	  //- code: ContentValues c = new ContentValues();
	  //- code: c.put(DB_PICTURE_DATA_KEY, StaticLocationDAObject.getIcon());
	  //- ...
	  
	  public void setAchievementTitle(String achievementTitle){
		  this.achievementTitle = achievementTitle;
	  }
	  
	  public void setAchiementDescription(String achievementDescription){
		  this.achievementDescription = achievementDescription;
	  }

	  public void setId(int i){
		  this.uniqueId = i;
	  }
	  public void setTimeAchievementWon(String timeAchievementWon) 
	  {
		  this.timeAchievementWon = timeAchievementWon;
	  }
	  public void setAchievementPoints(int achievementPoints) 
	  {
		  this.achievementPoints = achievementPoints;
	  }
	  
	  /* ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: ACCESSORS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	  
	  public int getId(){
		  return uniqueId;
	  }
	  
	  //STRING VALUE OF IMAGE< TO STORE IN DATABASE
	  public String getIconForDB(){
		  return icon;
	  }
	  
	  public Drawable getAchievementIcon()
	  {
		  return achievementIcon;
	  }
	  
	  public String getAchievementTitle()
	  {
		  return achievementTitle;
	  }
	  
	   public String getAchievementDescription()
	   {
		  return achievementDescription;
	  }
	public String getTimeAchievementWon() {
		return timeAchievementWon;
	}
	
	  
	public int getAchievementPoints() {
		return achievementPoints;
	}
	
}
