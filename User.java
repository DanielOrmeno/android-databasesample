package com.greenhills.database;

public class User {
	/*
	 * CLASS VARIABLES
	 */
	
	private String name;
	private String type;
	private String mobilePhone;
	private String sessionKey;
	private String userId;
	
	/*
	 * CONSTRUCTORS
	 */
	
	public User(){
		name="";
		type="";
		mobilePhone="null";
		sessionKey="";
		userId="";
	}
	
	public User(String name, String type, String phone){
		this.name=name;
		this.type=type;
		this.mobilePhone=phone;
		sessionKey="";
		userId="";
	}
	
	/*
	 * ACCESSORS 
	 */
	
	public String getName(){
		return name;
	}
	
	public String getType(){
		return type;
	}
	
	public String getMobilePhone(){
		return mobilePhone;
	}
	
	public String getSessionKey(){
		return sessionKey;
	}
	
	public String getUserId(){
		return userId;
	}
	
	/*
	 * MUTATORS
	 */
	
	public void setName(String newName){
		this.name=newName;
	}
	
	public void setType(String newType){
		this.type=newType;
	}
	
	public void setMobilePhone(String newPhone){
		this.mobilePhone=newPhone;
	}
	
	public void setSessionKey(String key){
		this.sessionKey=key;
	}
	
	public void setUserId(String id){
		this.userId=id;
	}
	
	/*
	 * CLASS METHODS
	 */
	
	@Override
	public String toString(){
		return " Name: "+name+" Type: "+type+" MobilePhone: "+mobilePhone;
	}
}
