package com.greenhills.database;

import java.util.ArrayList;
import java.util.List;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.greenhills.R;
import com.greenhills.services.ImageUtils;			//- Custom Class for converting and storing images in database.  http://stackoverflow.com/questions/10831151/how-to-store-and-retrieve-images-in-android-sqlite-database-and-display-them-on

import android.graphics.drawable.Drawable;

public class StaticLocation {

	/* ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: CLASS VARIABLES ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	private Drawable icon;
	
	private int uniqueId;
	private byte[] uuid;
	
	private String name;
	private String details;
	private String openTime;
	private String closeTime;
	private int duration;
	private int capacity;
	
	private LatLng latLong;
	private double latitude;
	private double longitude;
	
	/* ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: CONSTRUCTORS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	
	public StaticLocation()		//- Default Constructor
	{
		uniqueId=0;
		uuid = null;							//- Setting to null
		name= null;								//- Setting to null
		details= null;							//- Setting to null
		latitude= 0;							//- Setting to null
		longitude= 0;							//- Setting to null
		latLong=null;							//- Setting to null
		icon= null;								//- Setting to null
		openTime=null;							//- Setting to null
		closeTime=null;							//- Setting to null
		duration=0;
		capacity=0;
	}
	
	public StaticLocation(byte[] uuid, String name, String details, double latitude, double longitude, Drawable icon)		//- Constructor
	{
		this.uniqueId		=0;
		this.uuid 		= uuid;								//- Setting to 
		this.name 		= name;								//- Setting to 
		this.details 	= details;							//- Setting to 
		this.latitude 	= latitude;							//- Setting to 
		this.longitude 	= longitude;						//- Setting to
		this.latLong 	= new LatLng(latitude,longitude);	//- Setting latLong
		this.icon		= icon;								//- Setting to
		this.duration   =0;
		this.capacity   =0;
	}
	  
	
	public StaticLocation(byte[] uuid, String name, String details, double latitude, double longitude, Drawable icon, int duration, int capacity)		//- Constructor
	{
		this.uniqueId		=0;
		this.uuid 		= uuid;								//- Setting to 
		this.name 		= name;								//- Setting to 
		this.details 	= details;							//- Setting to 
		this.latitude 	= latitude;							//- Setting to 
		this.longitude 	= longitude;						//- Setting to
		this.latLong 	= new LatLng(latitude,longitude);	//- Setting latLong
		this.icon		= icon;								//- Setting to
		this.duration   =duration;
		this.capacity   =capacity;
	}
	  
	  
	  /* ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: MUTATORS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	  
	  public void setUUID(byte[] uuid){
		  this.uuid = uuid;
	  }
	  
	  public void setIcon(byte[] iconData) {
		  icon = ImageUtils.byteToDrawable(iconData);
	  }
	  
	  //- Wherever you retrieve your data from DB it'll be enough with adding something like that:
	  
	  //- code: user.setPictureData(cursor.getBlob(cursor.getColumnIndexOrThrow(DB_PICTURE_DATA_KEY)));
	  //- http://stackoverflow.com/questions/10831151/how-to-store-and-retrieve-images-in-android-sqlite-database-and-display-them-on
	  //-  For the opposite way (writing the drawable to DB)
	  //- code: ContentValues c = new ContentValues();
	  //- code: c.put(DB_PICTURE_DATA_KEY, StaticLocationDAObject.getIcon());
	  //- ...
	  
	  public void setName(String name){
		  this.name = name;
	  }
	  
	  public void setDetails(String details){
		  this.details = details;
	  }

	  public void setLatitude(double latitude){
		  this.latitude = latitude;
	  }
	  
	  public void setLongitude(double longitude){
		  this.longitude = longitude;
	  }
	  
	  public void setOpenTime(String ot){
		  this.openTime = ot;
	  }
	  
	  public void setCloseTime(String ot){
		  this.closeTime = ot;
	  }
	  
	  public void setLatLong(double lat, double lon){
		  this.latLong=new LatLng(lat,lon);
	  }
	  
	  public void setId(int i){
		  this.uniqueId=i;
	  }
	  
	  public void setCapacity(int i){
		  this.capacity=i;
	  }
	  
	  public void setDuration(int i){
		  this.duration=i;
	  }
	  
	  /* ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: ACCESSORS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	  
	  public int getId(){
		  return uniqueId;
	  }
	  
	  public byte[] getUUID(){
		  return uuid;
	  }
	  
	  public Drawable getIcon()
	  {
		  return icon;
	  }
	  
	  public String getName(){
		  return name;
	  }
	  
	   public String getDetails(){
		  return details;
	  }
	  
	  public double getLatitude(){
		  return latitude;
	  }
	  
	  public double getLongitude(){
		  return longitude;
	  }
	  
	  public String getOpenTime(){
		  return openTime;
	  }
	  
	  public String getCloseTime(){
		  return closeTime;
	  }
	  
	  public LatLng getLatLong(){
		  return latLong;
	  }
	  
	  public int getDuration(){
		  return this.duration;
	  }
	  
	  public int getCapacity(){
		  return this.capacity;
	  }
	  
	  /* ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: CLASS SPECIFIC METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	  
	  public Marker getStaticLocationMarker(GoogleMap map){
		  
		  Marker locationMarker = map.addMarker(new MarkerOptions().position(latLong).title(this.getName()).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_launcher)));
          
          return locationMarker;
	  }
	  
	  //METHOD DESIGNED ONLY FOR PLANNER.
	  

	  
}
