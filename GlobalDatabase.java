package com.greenhills.database;

import java.util.ArrayList;
import java.util.List;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class GlobalDatabase extends Application {
	
	/*
	 * ------------------------------ GLOBAL VARIABLES -------------------------------------------/
	 */
	
	private DatabaseHandler db;
	private User user;
	private String hashedPw;
	private String username;
	private List <PlannedStaticLocation> plannedAttractions;
	private boolean rotationLocked;
	
	/*
	 * ------------------------------ CLASS SPECIFIC METHODS ------------------------------------------/
	 */
	
	@Override
	public void onCreate() {
	db = new DatabaseHandler(getApplicationContext());
	user = new User();
	plannedAttractions = new ArrayList<PlannedStaticLocation>();
	hashedPw="";
	username="";
	rotationLocked=false;
	super.onCreate();
	}
	
	
	public void clearUserData(){
		
		this.db.dropTables();
		this.user=null; //Garbage Collector will delete as no other reference to old object.
		this.user=new User();
		this.plannedAttractions=null;
		this.plannedAttractions = new ArrayList<PlannedStaticLocation>();
		hashedPw="";
	}
	/*
	 * ------------------------------ ACCESSORS -------------------------------------------/
	 */
	
	public DatabaseHandler getDb(){
		return this.db;
	}
	
	public User getUser(){
		return this.user;
	}
	
	public List<PlannedStaticLocation> getPlannedAttractions(){
		return this.plannedAttractions;
	}
	
	public String getHashedPw(){
		return hashedPw;
	}
	
	public String getUsername(){
		return username;
	}
	
	public boolean getRotationLocked(){
		return rotationLocked;
	}
	
	/*
	 * ------------------------------ MUTATORS -------------------------------------------/
	 */
	
	public void setUser(User u){
		this.user=u;
	}
	
	public void setDb(DatabaseHandler d){
		this.db=d;
	}
	
	public void setPlannedAttractions(List <PlannedStaticLocation> list){
		this.plannedAttractions=list;
	}
	
	public void setHashedPw(String s){
		this.hashedPw=s;
	}
	
	public void setUsername(String s){
		this.username=s;
	}
	
	public void setRotationLocked(boolean b){
		this.rotationLocked=b;
	}
	
	
	/*
	 * ------------------------------ SERVER METHODS -------------------------------------------/
	 */
}
